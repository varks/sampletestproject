#include<stdint.h>

#define FS_SIZE 30 * 1024 * 1024

#define FS_BLOCK_SIZE 4096
#define MAX_NUM_BLOCKS FS_SIZE/FS_BLOCK_SIZE
#define MAX_DIRECTORY_ENTRIES_PER_BLOCK FS_BLOCK_SIZE/128
#define MAX_INODES (FS_BLOCK_SIZE/128)
#define MAX_NUMBER_BLOCKS FS_SIZE/FS_BLOCK_SIZE
#define NUM_DATA_BLOCKS 7680 - DATA_BLOCK_OFFSET
<<<<<<< HEAD
#define DATA_BLOCK_OFFSET 10 // be careful whil using this an index
=======
#define DATA_BLOCK_OFFSET 6 // be careful whil using this an index
>>>>>>> 622fdf1c2bb041d8fb2a603e7861a63e5283b7ce
#define MAX_DIRECTORY_NAME_LENGTH 64 
#define NUM_FREE_DATA_BLOCKS_PER_BLOCK FS_BLOCK_SIZE/4
 


#define REGULAR_FILE 1
#define DIRECTORY_FILE 2
#define SPECIAL_FILE 3
#define BLOCK_FILE 4

struct block {
	char data[FS_BLOCK_SIZE];
};

struct fs_freelist {
	struct block *blk;
	struct fs_freelist *blk_next;
};

struct fs_inode {
/*
	struct block *direct1;
	struct block *direct2;
	struct block *direct3;
	struct block *direct4;
	struct block *direct5;
	struct block *direct6;

	struct block *direct7;
	struct block *direct8;
	struct block *direct9;
	struct block *direct10;
	struct block **indirect1;
	struct block **indirect2;
*/
/* 84 bytes */
	uint32_t db1;
	uint32_t db2;
	uint32_t db3;
	uint32_t db4;
	uint32_t db5;
	uint32_t db6;
	uint32_t db7;
	uint32_t db8;
	uint32_t db9;
	uint32_t idb1;

	uint32_t free_flag;
	uint32_t ref_count;
	uint32_t uid;
	uint32_t gid;

	uint32_t mod_time;
	uint32_t change_time;
	uint32_t access_time;
	uint32_t file_type;
	
	uint32_t inode_num;
	uint32_t fs_filesize;

	char pad[48]; 
};

/* not sure if this can be maintained as an array */
struct fs_freeinode_list {
	struct fs_inode inode;
	struct fs_freeinode_list *next;

};
struct fs_superblock {
//  64 bytes
	
	uint64_t fs_size;
	uint64_t num_of_free_blks;
	uint32_t head_data;
	uint32_t tail_data;
	uint64_t num_of_free_inodes;
	uint32_t head_inode;
	uint32_t tail_inode;
	uint32_t superblock_mod; // not sure if reqd as of now
	char reserved[20];
};

struct fs_directory {
	char dir_name[MAX_DIRECTORY_NAME_LENGTH];
	uint32_t inode;
};



/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall `pkg-config fuse --cflags --libs` fusexmp.c -o fusexmp
*/

#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif
#include<string.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <strings.h>
#include <sys/mman.h>
#include <inttypes.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif
#include "fuse_ds_latest.h"
#include <stdlib.h>


int i;
void *p;
int fd;
/* Initialize superblock and freelist and inode */
struct fs_superblock sb;
struct fs_inode inode;
struct block data;
uint32_t free_list_map[NUM_DATA_BLOCKS]; 
int bytes = 0;

static const char *fs_str = "Hello World!\n";
static const char *fs_path = "/hello";

static int fs_getattr(const char *path, struct stat *stbuf)
{
        int res = 0;

        memset(stbuf, 0, sizeof(struct stat));
        if (strcmp(path, "/") == 0) {
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
        } else if (strcmp(path, fs_path) == 0) {
                stbuf->st_mode = S_IFREG | 0444;
                stbuf->st_nlink = 1;
                stbuf->st_size = strlen(fs_str);
        } else
                res = -ENOENT;

        return res;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi)
{
        (void) offset;
        (void) fi;

        if (strcmp(path, "/") != 0)
                return -ENOENT;

        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);
        filler(buf, fs_path + 1, NULL, 0);

        return 0;
}

static int fs_open(const char *path, struct fuse_file_info *fi)
{
        if (strcmp(path, fs_path) != 0)
                return -ENOENT;

        if ((fi->flags & 3) != O_RDONLY)
                return -EACCES;

        return 0;
}

<<<<<<< HEAD
static int fs_read(const char *path, char *buf, size_t size, off_t offset,
=======

static int hello_read(const char *path, char *buf, size_t size, off_t offset,
>>>>>>> 450750772a02dc05b98a2b26ec734367ce29ac28
                      struct fuse_file_info *fi)
{
        size_t len;
        (void) fi;
        if(strcmp(path, fs_path) != 0)
                return -ENOENT;

        system("echo 'fs world log33' >> /log2.txt");
        len = strlen(fs_str);
        if (offset < len) {
                if (offset + size > len)
                        size = len - offset;
                memcpy(buf, fs_str + offset, size);
        } else
                size = 0;

        return size;
}


static struct fuse_operations fs_oper = {
        .getattr        = fs_getattr,
        .readdir        = fs_readdir,
	.open           = fs_open,
    	.read           = fs_read,
};

void mmap_file() {

	fd = open("test.img", O_RDWR);
        /* memory mapped */
        p = mmap(NULL,0,PROT_NONE,0,fd,0);
}

void init_superblock() {

        /* Zeroing the structures just to be on safer side */
        bzero(&sb, sizeof(struct fs_superblock));
        bzero(&inode, sizeof(struct fs_inode));

        /* Setting lseek to beginning of file */
        if(lseek(fd, 0 , SEEK_SET) == -1)
                perror("\n Seek error\n");

        /* Initialize superblock */
        sb.fs_size = FS_SIZE;
        sb.num_of_free_blks = NUM_DATA_BLOCKS;
        sb.head_data = 0;
        sb.num_of_free_inodes = MAX_INODES;
        sb.head_inode = 0;
        sb.superblock_mod = 0;
        /* write superblock to block 0 */
	
	/* write superblock to block 0 */
       if(write(fd,&sb,sizeof(struct fs_superblock)) != sizeof(sb))
                printf("\n Error writing Superblock \n");
}

void init_inodeblock() {
      	lseek(fd, 0, SEEK_SET);
        lseek(fd, FS_BLOCK_SIZE, SEEK_SET);

        /* Write INODES to block 1 */
        for(i= 0; i < MAX_INODES; i++) {
                inode.inode_num = i + 1;
                inode.free_flag = 1;
                if(write(fd, &inode, sizeof(struct fs_inode)) != sizeof(inode))
                        printf("\n Error writing Inode \n");
                bytes += sizeof(inode);
        }
        printf("\n Bytes read %d\n", bytes);
}

void init_freelists()
{
	for (i = 0; i < NUM_DATA_BLOCKS ; i++) {
                free_list_map[i] = i + 1;
        }

	lseek(fd, 0, SEEK_SET);
        lseek(fd, 2 * FS_BLOCK_SIZE, SEEK_SET);
        
	/* Writing list of free data blocks to Block 2 */
       if(write(fd, free_list_map, sizeof(free_list_map)) != sizeof(free_list_map))
                printf("\n Error writing free list map\n");
}

void init_datablocks()
{
       lseek(fd, 0, SEEK_SET);
        lseek(fd, 10 * FS_BLOCK_SIZE, SEEK_SET);

        /* Writing all data blocks to raw DISK  AKA FaaFS ( File as a FileSystem */
        for(i=0; i < NUM_DATA_BLOCKS; i++) {
        //      sprintf(data.data,"fs%d", i+1);      
       		if(write(fd, &data, sizeof(struct block)) != sizeof(struct block))
			printf("\n Error writing data block %d\n", i);
        }
}

void display_superblock()
{
struct fs_superblock sb;

/* Setting lseek to beginning of file */
if(lseek(fd, 0 , SEEK_SET) == -1)
        perror("\n Seek error\n");

if(read(fd, &sb, sizeof(sb)) < 0)
        printf("read was unsuccessful");

printf("-- SuperBlock --\n");
printf("\n Free Blocks %" PRId64 "\n", sb.num_of_free_blks);
printf("\n FS size %" PRId64 "\n", sb.fs_size);
printf("\n Free Inodes %" PRId64 "\n", sb.num_of_free_inodes); 
printf("------------\n");
}


struct fs_inode * allocate_freeblock(uint32_t file_type) {
struct fs_superblock sb;
struct fs_inode *inode= (struct fs_inode *)malloc(sizeof(struct fs_inode));
uint32_t free_list_map[NUM_DATA_BLOCKS];

printf("Reading Superblock\n");
if(lseek(fd, 0 , SEEK_SET) == -1)
  	perror("\n Seek error\n");

if(read(fd, &sb,sizeof(struct fs_superblock)) != sizeof(struct fs_superblock))
	printf("\nread error\n");

printf(" Find first free inode block\n");
lseek(fd, 0, SEEK_SET);
lseek(fd, FS_BLOCK_SIZE, SEEK_SET);

int inode_idx = 0;
for(inode_idx=0; inode_idx< MAX_INODES; inode_idx++)
{
if(read(fd, inode, sizeof(struct fs_inode)) != sizeof(struct fs_inode))
	printf("\n Error reading Inode \n");
if(inode->free_flag ==1)
	break;
}

if(inode_idx == MAX_INODES)
    printf("No more free inodes");

printf(" Find first free list block\n");
lseek(fd, 0, SEEK_SET);
lseek(fd, 2 * FS_BLOCK_SIZE, SEEK_SET);
        
if(read(fd, free_list_map, sizeof(free_list_map)) != sizeof(free_list_map))
  printf("\n Error reading free list map\n");

int index = 0;
for (index = 0; index < NUM_DATA_BLOCKS && free_list_map[index]==0; index++);

if (index == NUM_DATA_BLOCKS)
	printf("Free list map is full");

printf(" Updating free list \n");
free_list_map[index]=0;
lseek(fd, 0, SEEK_SET);
lseek(fd, 2 * FS_BLOCK_SIZE, SEEK_SET);

if(write(fd, free_list_map, sizeof(free_list_map)) != sizeof(free_list_map))
     printf("\n Error writing free list map\n");

printf("Updating inode list \n");
inode->db1 = index; inode->free_flag = 0;
inode->ref_count=1;inode->uid = 0; inode->gid = 0;
inode->file_type = file_type;
inode->fs_filesize = FS_BLOCK_SIZE;// assuming file size = block size 

lseek(fd, 0, SEEK_SET);
lseek(fd, FS_BLOCK_SIZE+sizeof(struct fs_inode)*inode_idx, SEEK_SET);

if(write(fd, inode, sizeof(struct fs_inode))!= sizeof(struct fs_inode))
	printf("\n Error writing inode map\n");

printf("Updating superblock \n");
sb.num_of_free_blks--;
sb.num_of_free_inodes--;

if(lseek(fd, 0 , SEEK_SET) == -1)
    perror("\n Seek error\n");

if(write(fd,&sb, sizeof(struct fs_superblock)) != sizeof(sb))
      printf("\n Error writing Superblock \n");
return inode;
}

static int fs_mkdir(const char *path , mode_t m){

printf ("Find data block index");
struct fs_inode *inode = allocate_freeblock((uint32_t) 2);

uint32_t db_idx = inode->db1;
lseek(fd, 0, SEEK_SET);
lseek(fd, (10+db_idx) * FS_BLOCK_SIZE, SEEK_SET);

struct fs_directory dir;
strncpy(dir.dir_name, path, 60);
dir.inode = inode->inode_num;

if(write(fd, &dir, sizeof(struct fs_directory)) != sizeof(struct fs_directory))
	printf("\n Error writing data block %d\n", i);

return 0;
}

void mkfs()
{
	mmap_file();
	init_superblock();
	display_superblock();
	init_inodeblock();
	init_freelists();
	init_datablocks();
	uint32_t ft = 2;
	allocate_freeblock(ft);
}

int main(int argc, char*argv[]) {
	/* Make file system */
	mkfs();		                                            
	return fuse_main(argc, argv, &fs_oper, NULL);
}
